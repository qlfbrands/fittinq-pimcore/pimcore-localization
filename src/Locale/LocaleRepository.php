<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Localization\Locale;

use Pimcore\Tool;

class LocaleRepository
{
    /**
     * @return Locale[]
     */
    public function getLocales(): array
    {
        $locales = [];

        foreach (Tool::getValidLanguages() as $language) {
            $locales[] = new Locale($language);
        }

        return $locales;
    }

    public function getLocale(string $name): ?Locale
    {
        foreach (Tool::getValidLanguages() as $language) {
            if ($language === $name) {
                return new Locale($language);
            }
        }

        return null;
    }
}
