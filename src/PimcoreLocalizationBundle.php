<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Localization;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PimcoreLocalizationBundle extends Bundle
{
}
