<?php declare(strict_types=1);

use Pimcore\Bootstrap;
use Pimcore\Model\DataObject\Localizedfield;
use Symfony\Component\Dotenv\Dotenv;

include "vendor/autoload.php";


Bootstrap::setProjectRoot();

if (class_exists('Symfony\Component\Dotenv\Dotenv')) {
    (new Dotenv())->bootEnv(PIMCORE_PROJECT_ROOT . '/.env');
}
Bootstrap::startupCli();

// Disabling the fallback mechanism for localized fields to ensure our tests evaluate the explicit values set during testing.
Localizedfield::setGetFallbackValues(false);
